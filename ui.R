
library(shinyjs)

# Define UI for application that plots random distributions 
shinyUI(fluidPage(
  
  # App title ----
  titlePanel("NN configurer"),
  
  tags$head(tags$style(
    type="text/css",
    "#myImage img {max-width: 100%; width: 100%; height: auto}"
  )),
  
  # Sidebar layout with input and output definitions ----
  sidebarLayout(
    
    # Sidebar panel for inputs ----
    sidebarPanel(
      
      # Input: Select a file ----
      fileInput("file1", "Choose File (*)",
                multiple = FALSE,
                accept = c("text/csv",
                           "text/comma-separated-values,text/plain",
                           ".csv")),
      verbatimTextOutput("note"),
      # Horizontal line ----
      tags$hr(),
      
      # Input: Checkbox if file has header ----
      checkboxInput("header", "Header", TRUE),
      
      # Input: Select separator ----
      radioButtons("sep", "Separator",
                   choices = c(Comma = ",",
                               Semicolon = ";",
                               Tab = "\t"),
                   selected = ","),
      
      # Input: Select quotes ----
      radioButtons("quote", "Quote",
                   choices = c(None = "",
                               "Double Quote" = '"',
                               "Single Quote" = "'"),
                   selected = '"'),
      
      # Horizontal line ----
      tags$hr(),
      
      # Input: Select number of rows to display ----
      radioButtons("disp", "Display",
                   choices = c(Head = "head",
                               All = "all"),
                   selected = "head"),
      hidden(
        div(id='text_div',
            verbatimTextOutput("text"), 
            tableOutput("downloadData1")
        )
      ),
      br(),
      hidden(
        div(id='text_div2',
            verbatimTextOutput("text2"), 
            tableOutput("downloadData2")
        )
      )
    )
    ,
    
    
    
    # Main panel for displaying outputs ----
    mainPanel(
      imageOutput("myImage",height="100%"),
      br(),
      # Output: Data file ----
      tableOutput("contents"),
      tableOutput("submitBut"),
      hidden(
        div(id='mf1',
            div(style="display: inline-block;vertical-align:top; width: 150px;",verbatimTextOutput("message")),
            div(style="display: inline-block;vertical-align:top; width: 150px;",uiOutput("icon"))
        )
      ),
      hidden(
        div(id='mtf',tableOutput("metafeatures"))
      ),
      hidden(
        div(id='param_but',
            div(style="display: inline-block;vertical-align:top; width: 150px;",tableOutput("ParameteriseBut")),
            div(style="display: inline-block;vertical-align:top; ",verbatimTextOutput("parameterisation")
            )
        ),
        hidden(
          div(id='param_div',
              tableOutput("wts2")
          )
        ),
        hidden(
          div(id='initi_but',
              div(style="display: inline-block;vertical-align:top; width: 150px;",tableOutput("initialiseBut"))
          )
        ),
        hidden(
          div(id='impacts',
              DT::dataTableOutput("mytable"))
        ),
        hidden(
          div(id='getwts',
              div(style="display: inline-block;vertical-align:top; width: 150px;",tableOutput("wts1")), 
              div(style="display: inline-block;vertical-align:top; width: 150px;",tableOutput("getWeights"))
          )
        ),
        hidden(
          div(id='wts',
              verbatimTextOutput("weights")
          )
        )
      )
    )
  )
))
